puts "Star Patter A"
n = 1
star="*"
while n < 6
    puts star*n
    n+=1
end

puts "--------------------------------"

puts "Star Patter B"
n = 5
star="*"
while n > 0
    puts star*n
    n-=1
end

puts "--------------------------------"

puts "Star Patter C"
n = 5
star="*"
while n > 0
    puts " "*n+star
    star+="*"
    n-=1
end

puts "--------------------------------"

puts "Star Patter D"
n = 5
star="*"
space=""
while n > 0
    puts space+star*n
    space+=" "
    n-=1
end