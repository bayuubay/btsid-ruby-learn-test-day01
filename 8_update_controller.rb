# class ArticlesController < ApplicationController
#   def update
#     article = Article.find(params[:id])
#     article.update(form_params)
#     if article.genre == 'Pop'
#       article.publish_date = Date.today + 1.month
#       article.status = 'pending_review'
#     elsif article.genre == 'Animals'
#       article.publish_date = Date.today + 2.weeks
#       article.status = 'pending_approval'
#     end
#     article.save!

#       if article.status == 'pending_review'
#         writer = article.writer
#         if writer.articles.count > 10
#           writer.status = 'pending_upgrade_review'
#           writer.save!
#           SendWriterUpgradeMail.perform_now(writer)
#         end
#       end
#   end
# end

# the preferable change is something like this

# put model logic to model file

class ArticlesController < ApplicationController
  def update
    article = Article.find(params[:id])
    article.publish_status
    article.update(form_params)
    article.save!
    if article.status == 'pending_review'
      writer = article.writer
      if writer.articles.count > 10
        writer.status = 'pending_upgrade_review'
        writer.save!
        SendWriterUpgradeMail.perform_now(writer)
      end
    end
  end
end

class Article < ApplicationRecords
  before_update :publish_status

  def publish_status
    if self.genre == 'Pop'
      self.publish_date = Date.today + 1.month
      self.status = 'pending_review'
    elsif self.genre == 'Animals'
      self.publish_date = Date.today + 2.weeks
      self.status = 'pending_approval'
    end
  end
end