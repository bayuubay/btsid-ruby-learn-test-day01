articles = Articles.joins(:comments)
                  .where("created_at >= ? AND created_at < ?", date1, date2)
                  .where("publish_date >= #{params[:date]}")
                  .where(genre: 'Pop')
                  .where("comments.created_at >= #{params[:date]}")
                  .select('articles.title')


# there's something wring with the statement above.
# the correct statement is more like this:

articles = Articles.select('articles.title')
                  .where("created_at >= ? AND created_at < ?", date1, date2)
                  .where("publish_date >= #{params[:date]}")
                  .where(genre: 'Pop')
                  .joins(:comments)
                  .where("comments.created_at >= #{params[:date]}")

# the statement above will return articles title that created between date1 to date 2
# and published date same or after desired date inputted into params
# it will filter the articles that have genre of pop
# is will also return all the comments that related to the articles that dated same or after desired date. 
